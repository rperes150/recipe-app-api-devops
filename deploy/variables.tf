variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "rui@ruiperes.com"
}

variable "db_username" {
  description = "Username for the RDS postgress instance"
}

variable "db_password" {
  description = "Password for the RDS prosgres instance"
}

variable "bastion_key_name" {
  # default = "recipe-app-api-devops-bastion"
  default = "raad"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "272004770637.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "272004770637.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:1.0.0:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
